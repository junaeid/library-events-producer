package com.springboot.kafka.libraryinventory.app.domain;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Book {
    private Long bookId;
    private String bookName;
    private String bookAuthor;
}
