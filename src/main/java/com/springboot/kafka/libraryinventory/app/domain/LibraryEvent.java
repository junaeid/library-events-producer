package com.springboot.kafka.libraryinventory.app.domain;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LibraryEvent {
    private Long libraryEventId;
    private Book book;
}
